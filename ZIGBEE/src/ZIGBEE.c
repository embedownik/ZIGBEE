#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <FreeRTOS.h>
#include <task.h>

#include <ETRX357.h>
#include <ETRX357_registers.h>

#include <ZIGBEE.h>
#include <ZIGBEE_Response.h>

/* common modules - SRP quite broken, but all modules are only for Pyrolligent */
#include <sound.h>
#include <status.h>
#include <flash.h>

#include <utils.h>

#include <strings.h>

#include <board.h>

/* max number of networks during scan */
#define ZB_NETWORK_FOUND_MAX (5U)

static ZB_NetworkSettings_s actualNetworkSettings = { 0 };

//! aktualnie uzywany nadajnik - do niego raportujemy/odsylamy okejki
//! podmieniany w przypadku przelaczenia sie na nadajnik poboczny
static MAC *actualMasterMAC = (MAC*)actualNetworkSettings.mainMaster;

/**
 * W przypadku komend z unicastem - okejki moga trafic do innego urzadzenia.
 * Zapisujemy tutaj tymczasowo jego MAC.
 * Dodatkowo rzeczy z ZB z akcja i wyslaniem OKejki wydarza sie w obrebie jednego
 * taska - wiec nie trzeba tego przypisywac do poszczegolnych odpowiedzi - tylko do aktualnej.
 */
static MAC *actualCommandConfirmMaster = (MAC*)actualNetworkSettings.mainMaster;

static ZB_ConnectionState_e actualConnectionState = ZB_ConnectionState_NoNetwork;

/**
 * Disconnection detect counter - clear in case of communication with network.
 */
static uint16_t networkTimeoutCounter = 0;

/**
 * Pointer to struct with all actual used network callbacks.
 */
static ZB_EventsCallbacks_s *callbacks = NULL;

void ZB_RegisterCallbacks(ZB_EventsCallbacks_s *events)
{
    callbacks = events;
}

void ZB_ClearConnectionStatusCounter(void)
{
    actualConnectionState = ZB_ConnectionState_OK;
    networkTimeoutCounter = 0U;
}

static bool compareMACsEqual(const char *MAC1, const char *MAC2)
{
    bool retVal = false;

    if(strncmp(MAC1, MAC2, sizeof(MAC)) == 0)
    {
        retVal = true;
    }

    return retVal;
}

bool ZB_CheckIfGivenMACAddressIsMaster_WithMasterChange(const MAC *mac)
{
    bool retVal = false;

    if(actualMasterMAC != NULL && memcmp(actualMasterMAC, mac, sizeof(MAC)) == 0)
    {
        ZB_DEBUG_PRINT("MSG OK");
        retVal = true;
    }
    else
    {
        bool MAC_found = false;

        if(compareMACsEqual((char*)actualNetworkSettings.mainMaster, (char*)mac))
        {
            ZB_DEBUG_PRINT("MAIN MASTER!");
            actualMasterMAC = (MAC*)actualNetworkSettings.mainMaster;
            MAC_found = true;
        }
        else if(compareMACsEqual((char*)actualNetworkSettings.secondaryMaster_1, (char*)mac))
        {
            ZB_DEBUG_PRINT("SECONDARY MASTER 1!");
            actualMasterMAC = (MAC*)actualNetworkSettings.secondaryMaster_1;
            MAC_found = true;
        }
        else if(compareMACsEqual((char*)actualNetworkSettings.secondaryMaster_2, (char*)mac))
        {
            ZB_DEBUG_PRINT("SECONDARY MASTER 2!");
            actualMasterMAC = (MAC*)actualNetworkSettings.secondaryMaster_2;
            MAC_found = true;
        }
        else if(compareMACsEqual((char*)actualNetworkSettings.secondaryMaster_3, (char*)mac))
        {
            ZB_DEBUG_PRINT("SECONDARY MASTER 3!");
            actualMasterMAC = (MAC*)actualNetworkSettings.secondaryMaster_3;
            MAC_found = true;
        }

        if(MAC_found)
        {
            if(callbacks->zb_cb_MasterChanged)
            {
                callbacks->zb_cb_MasterChanged();
            }

            retVal = true;
        }
    }

    if(retVal == false)
    {
        ZB_DEBUG_PRINT("MESSAGE FROM UNKNOWN DEVICE!");
    }

    return retVal;
}

bool ZB_CheckIfGivenMACAddressIsMaster_WithoutMasterChange(const MAC *mac)
{
    bool retVal = false;

    if(memcmp(actualCommandConfirmMaster, mac, sizeof(MAC)) == 0)
    {
        ZB_DEBUG_PRINT("MSG OK");
        retVal = true;
    }
    else
    {
        bool MAC_found = false;

        if(compareMACsEqual((char*)actualNetworkSettings.mainMaster, (char*)mac))
        {
            ZB_DEBUG_PRINT("MAIN MASTER!");
            actualCommandConfirmMaster = (MAC*)actualNetworkSettings.mainMaster;
            MAC_found = true;
        }
        else if(compareMACsEqual((char*)actualNetworkSettings.secondaryMaster_1, (char*)mac))
        {
            ZB_DEBUG_PRINT("SECONDARY MASTER 1!");
            actualCommandConfirmMaster = (MAC*)actualNetworkSettings.secondaryMaster_1;
            MAC_found = true;
        }
        else if(compareMACsEqual((char*)actualNetworkSettings.secondaryMaster_2, (char*)mac))
        {
            ZB_DEBUG_PRINT("SECONDARY MASTER 2!");
            actualCommandConfirmMaster = (MAC*)actualNetworkSettings.secondaryMaster_2;
            MAC_found = true;
        }
        else if(compareMACsEqual((char*)actualNetworkSettings.secondaryMaster_3, (char*)mac))
        {
            ZB_DEBUG_PRINT("SECONDARY MASTER 3!");
            actualCommandConfirmMaster = (MAC*)actualNetworkSettings.secondaryMaster_3;
            MAC_found = true;
        }

        if(MAC_found)
        {
            retVal = true;
        }
    }

    if(retVal == false)
    {
        ZB_DEBUG_PRINT("MESSAGE FROM UNKNOWN DEVICE!");
    }

    return retVal;
}

static void ZB_ETRX_CB_joined_to_network(void)
{
    if(actualConnectionState != ZB_ConnectionState_MASTER)
    {
        if(actualConnectionState == ZB_ConnectionState_Searching || actualConnectionState == ZB_ConnectionState_Creating)
        {
            ZB_DEBUG_PRINT("Dolaczylismy do sieci\r\n");

            actualNetworkSettings.ID = 0;
            actualConnectionState = ZB_ConnectionState_NO_MASTER;

            ETRX357_CopyPID(actualNetworkSettings.myNetwork, ETRX357_GetNetworkInfoAfterJoin()->network_PID);

            ZB_SaveNetworkSettingToNVS();

            if(callbacks->zb_cb_NetworkConnected)
            {
                callbacks->zb_cb_NetworkConnected();
            }

            sound_Play(sound_type_ok);
        }
    }
}

/**
 * Otrzymanie broadcastu z sieci ZigBee. Przekazanie do warstwy wyzej.
 */
static void ZB_ETRX_CB_broadcast(MAC *mac, char *wsk)
{
    ZB_DEBUG_PRINT("Otrzymalismy BROADCAST!!!\r\n");
    ZB_DEBUG_PRINT("Od: %s\r\n", mac);
    ZB_DEBUG_PRINT("Taki: %s\r\n", wsk);

    if(callbacks->zb_cb_BroadcastReceived)
    {
        callbacks->zb_cb_BroadcastReceived(mac, wsk);
    }
}

/**
 * Get myMAC from module.
 */
static void getMyMAC_Command(void)
{
    if(actualNetworkSettings.myMAC[0] == 0xff || actualNetworkSettings.myMAC[0] == 0x00)
    {
        ETRX357_GetMAC_Command();
    }
}

/**
 * Otrzymanie broadcastu z sieci ZigBee. Przekazanie do warstwy wyzej.
 */
static void ZB_ETRX_CB_unicast(MAC *mac, char *wsk)
{
    ZB_DEBUG_PRINT("UNICAST!!!\r\n");
    ZB_DEBUG_PRINT("From: %s\r\n", mac);
    ZB_DEBUG_PRINT("This->%s\r\n", wsk);

    if(callbacks->zb_cb_UnicastReceived)
    {
        callbacks->zb_cb_UnicastReceived(mac, wsk);
    }
}

static void ZB_ETRX_CB_leave_network(void)
{
    ZB_DEBUG_PRINT("Opuscilismy siec");

    ETRX357_AutojoinDisable();
}

static void ZB_ETRX_CB_get_reg_value(uint8_t reg, uint8_t *values, uint8_t length)
{
    ZB_DEBUG_PRINT("Otrzymalem wartosc rejestru");
}

static void ZB_ETRX_CB_get_module_MAC(void)
{
    ZB_DEBUG_PRINT("Otrzymalem MAC - %s", ETRX357_GetMAC_AfterCommand());
    strcpy(actualNetworkSettings.myMAC, (char*)ETRX357_GetMAC_AfterCommand());

    uint8_t ID = ZB_GetID();
    if(ID == 0U)
    {
        actualNetworkSettings.ID = 0xffff;
    }

    ZB_SaveNetworkSettingToNVS();

    actualNetworkSettings.ID = ID;
}

static void ZB_ETRX_CB_get_network_info_callback(ZIGBEE_NETWORK_PARAMS *params)
{
    ZB_DEBUG_PRINT(0, "Kanal: %d\r\n", params->channel);
    ZB_DEBUG_PRINT(0, "PID: %s\r\n", params->network_PID);
    ZB_DEBUG_PRINT(0, "EPID: %s\r\n", params->network_EPID);
    ZB_DEBUG_PRINT(0, "Stack: %d\r\n", params->ZB_stack_profile);
    ZB_DEBUG_PRINT(0, "Joinable: %d\r\n", params->ZB_joining_permitted);

    if(callbacks->zb_cb_NetworkParams)
    {
        callbacks->zb_cb_NetworkParams(params);
    }
}

static uint8_t networksFoundDuringScanCounter = 0;
static ZIGBEE_NETWORK_PARAMS networksInRange[ZB_NETWORK_FOUND_MAX];

static void ZB_ETRX_CB_network_scan_get_network(ZIGBEE_NETWORK_PARAMS *params)
{
    ZB_DEBUG_PRINT("Kanal: %d\r\n", params->channel);
    ZB_DEBUG_PRINT("PID: %s\r\n", params->network_PID);
    ZB_DEBUG_PRINT("EPID: %s\r\n", params->network_EPID);
    ZB_DEBUG_PRINT("Stack: %d\r\n", params->ZB_stack_profile);
    ZB_DEBUG_PRINT("Joinable: %d\r\n", params->ZB_joining_permitted);

    if(networksFoundDuringScanCounter < ZB_NETWORK_FOUND_MAX)
    {
        ETRX357_Util_CopyNetworkParam(&(networksInRange[networksFoundDuringScanCounter]), params);
        networksFoundDuringScanCounter++;
    }
}

static void ZB_ETRX_CB_network_scan_get_network_completed(void)
{
    ZB_DEBUG_PRINT("Zakonczono skanowanie sieci. Znaleziono: %d\r\n", networksFoundDuringScanCounter);

    if(callbacks->zb_cb_NetworkScanFinished)
    {
        callbacks->zb_cb_NetworkScanFinished(networksInRange, networksFoundDuringScanCounter);
    }
}

static void ZB_ETRX_CB_new_device_joined(MAC *mac)
{
    ZB_DEBUG_PRINT("Nowe urzadzenie: %s\r\n", mac);
    if(callbacks->zb_cb_NewDeviceInNetwork)
    {
        callbacks->zb_cb_NewDeviceInNetwork(mac);
    }
}

static void ZB_ETRX_CB_network_join_fail(void)
{
    ZB_DEBUG_PRINT("Dolaczenie do sieci sie nie powiodlo\r\n");
    actualConnectionState = ZB_ConnectionState_NoNetwork;

    if(callbacks->zb_cb_ConnectionFailed)
    {
        callbacks->zb_cb_ConnectionFailed();
    }
}

/**
 * ETRX357 configuration.
 */
static ETRX357_InitParameters_s ETRX357_init_str =
{
    .send_rn = NULL, //! filled in Init function

    .cb_getRegValue = ZB_ETRX_CB_get_reg_value,

    .cb_get_MAC = ZB_ETRX_CB_get_module_MAC,

    .cb_broadcast = ZB_ETRX_CB_broadcast,
    .cb_unicast = ZB_ETRX_CB_unicast,

    .cb_get_network_info = ZB_ETRX_CB_get_network_info_callback,

    .cb_leave_network = ZB_ETRX_CB_leave_network,
    .cb_joined_to_network = ZB_ETRX_CB_joined_to_network,

    .cb_network_scan_found = ZB_ETRX_CB_network_scan_get_network,
    .cb_network_scan_completed = ZB_ETRX_CB_network_scan_get_network_completed,

    .cb_new_device_joined = ZB_ETRX_CB_new_device_joined,

    .cb_network_join_fail = ZB_ETRX_CB_network_join_fail,
};

ZB_ConnectionState_e ZB_GetActualConnectionState(void)
{
    return actualConnectionState;
}

void ZB_SetConnectionState(ZB_ConnectionState_e state)
{
    actualConnectionState = state;
}

static void getNetworkSettingsFromNVM(void)
{
    memcpy((void*)&actualNetworkSettings, (void*)ZIGBEE_FLASH_SETTINGS_ADDRESS, sizeof(ZB_NetworkSettings_s));
}

/**
 * Init ZB with data from memory.
 */
static void setDefaultNetworkState(void)
{
    void clearAllFields(void)
    {
        if(actualNetworkSettings.ID == 0xffff)
        {
            actualNetworkSettings.ID = 0;
        }
        if(actualNetworkSettings.myMAC[0] == 0xff)
        {
            actualNetworkSettings.myMAC[0] = 0;
        }
        if(actualNetworkSettings.mainMaster[0] == 0xff)
        {
            actualNetworkSettings.mainMaster[0] = 0;
        }
        if(actualNetworkSettings.secondaryMaster_1[0] == 0xff)
        {
            actualNetworkSettings.secondaryMaster_1[0] = 0;
        }
        if(actualNetworkSettings.secondaryMaster_2[0] == 0xff)
        {
            actualNetworkSettings.secondaryMaster_2[0] = 0;
        }
        if(actualNetworkSettings.secondaryMaster_3[0] == 0xff)
        {
            actualNetworkSettings.secondaryMaster_3[0] = 0;
        }
        if(actualNetworkSettings.myNetwork[0] == 0xff)
        {
            actualNetworkSettings.myNetwork[0] = 0;
        }
    }

    if(actualNetworkSettings.ID != 0xffff)
    {
        if(actualNetworkSettings.ID == ZB_NETWORK_MASTER_ID)
        {
            actualConnectionState = ZB_ConnectionState_MASTER;
        }
        else
        {
            if(actualNetworkSettings.mainMaster[0] != 0xff && actualNetworkSettings.mainMaster[0] != 0x00)
            {
                actualConnectionState = ZB_ConnectionState_MasterSearching;
            }
            else
            {
                actualConnectionState = ZB_ConnectionState_NO_MASTER;
            }
        }
    }
    else
    {
        actualConnectionState = ZB_ConnectionState_NoNetwork;
    }

    clearAllFields();

    ZB_DEBUG_PRINT("ZB STATE -> %d\r\n", actualConnectionState);
}

void ZB_SaveNetworkSettingToNVS(void)
{
    uint32_t *ptr = (uint32_t*)(&actualNetworkSettings);
    uint16_t tmp;

    flash_Unlock();

    flash_ErasePage(ZIGBEE_FLASH_SETTINGS_ADDRESS);

    __disable_fault_irq();
    for(tmp = 0U; tmp < sizeof(ZB_NetworkSettings_s); tmp += 4U)
    {
        flash_Write(ZIGBEE_FLASH_SETTINGS_ADDRESS + tmp, *ptr);
        ptr++;
    }
    __enable_fault_irq();

    flash_Lock();
}

void ZB_ScanNetworks_Command(void)
{
    if(actualConnectionState == ZB_ConnectionState_NoNetwork)
    {
        networksFoundDuringScanCounter = 0;

        actualConnectionState = ZB_ConnectionState_Searching;

        ETRX357_ScanNetworksStart();
    }
}

void ZB_ConnectWithSelectedNetworkIndex(uint8_t index)
{
    if(index < networksFoundDuringScanCounter)
    {
        ETRX357_Network_Join(&(networksInRange[index]));
    }
}

void ZB_SettingsClearInNVS(void)
{
    actualNetworkSettings.ID = 0xffff;

    for(uint8_t i = 0U; i < sizeof(MAC); i++)
    {
        actualNetworkSettings.mainMaster[i] = 0xff;
        actualNetworkSettings.secondaryMaster_1[i] = 0xff;
        actualNetworkSettings.secondaryMaster_2[i] = 0xff;
        actualNetworkSettings.secondaryMaster_3[i] = 0xff;
    }

    ZB_SaveNetworkSettingToNVS();
}

void ZB_Disconnect(void)
{
    actualConnectionState = ZB_ConnectionState_NoNetwork;

    ETRX357_Network_Leave();

    vTaskDelay(10);

    ETRX357_AutojoinDisable();

    vTaskDelay(10);

    ZB_SettingsClearInNVS();

    vTaskDelay(10);

    ZB_Response_Disable();
}

/**
 * funkcja ustawienie ID urzadzenia i zapis w pamieci FLASH
 * @param ID ID do ustawienia
 */
void ZB_SaveIDInNVS(uint16_t ID)
{
    actualNetworkSettings.ID = ID;

    ZB_SaveNetworkSettingToNVS();
}

void ZB_SaveMasterMACAndID(uint16_t ID, const MAC *mac)
{
    actualNetworkSettings.ID = ID;

    memcpy(actualNetworkSettings.mainMaster, (uint8_t*)mac, sizeof(MAC));

    ZB_SaveNetworkSettingToNVS();
}

void ZB_GetNetworkData_Command(ZB_cb_NetworkParams_t callback)
{
    callbacks->zb_cb_NetworkParams = callback;

    ETRX357_GetNetworkInfo_Command();
}

/**
 * Detecting of networkLost event.
 */
static void ZB_eventNetworkTimeout(void)
{
    networkTimeoutCounter++;

    if(networkTimeoutCounter >= (ZB_DISCONNECT_TIME_MS / ZB_DEF_DISCONNECT_TIMER_INTERVAL_MS))
    {
        if(actualConnectionState == ZB_ConnectionState_OK || actualConnectionState == ZB_ConnectionState_Searching)
        {
            actualConnectionState = ZB_ConnectionState_Lost;

            if(callbacks->zb_cb_Timeout)
            {
                callbacks->zb_cb_Timeout();
            }
        }
    }
}

void ZB_CheckEvents(void)
{
    //! jesli jest cos do sprawdzenia ( powinnismy byc polaczeni z jakas siecia + miec ja skonfigurowana )
    if(actualConnectionState != ZB_ConnectionState_NoNetwork && actualConnectionState != ZB_ConnectionState_Searching && actualConnectionState != ZB_ConnectionState_NO_MASTER)
    {
        ZB_eventNetworkTimeout();
    }
}

void ZB_SendMessageToMaster(const char *wsk)
{
    if((MAC*)actualMasterMAC[0] != 0)
    {
        ETRX357_SendUnicast((MAC*)actualMasterMAC, wsk);
    }
}

void ZB_SendMessageToSecondaryMaster(const char *wsk)
{
    if((MAC*)actualMasterMAC[0] != 0)
    {
        ETRX357_SendUnicast((MAC*)actualCommandConfirmMaster, wsk);
    }
}

void ZB_SayHelloToMaster_TEST(void)
{
    char responseBuff[50];

    char statusBuff[STATUS_MAX_LEN];

    status_GetStatusText(statusBuff);

    sprintf(responseBuff, "+HI1=%02d,%s", actualNetworkSettings.ID, statusBuff);

    ZB_SendMessageToMaster(responseBuff);
}

static void sayHello(void)
{
    char responseBuff[50];

    char statusBuff[STATUS_MAX_LEN];

    status_GetStatusText(statusBuff);

    sprintf(responseBuff, "+HI2=%02d,%s", actualNetworkSettings.ID, statusBuff);

    ZB_SendMessageToMaster(responseBuff);
}

void ZB_SayHelloToMaster(void)
{
    if(actualConnectionState != ZB_ConnectionState_MASTER)
    {
        if(actualConnectionState == ZB_ConnectionState_MasterSearching)
        {
            sayHello();
        }
    }
}

void ZB_SayHelloToMasterResponse(void)
{
    sayHello();
}

void ZB_Init(ZB_cb_SendMSGToETRX_rn_t send_rn)
{
    ETRX357_init_str.send_rn = send_rn;

    getNetworkSettingsFromNVM();

    setDefaultNetworkState();

    ETRX357_Init(&ETRX357_init_str);

    ZB_Response_Init();
}

void ZB_StartTask(void)
{
    getMyMAC_Command();

    vTaskDelay(20);

    //! jesli jest to pierwsze uruchomienie - kilka konfiguracji modulu:
    //! aczkolwiek powinno to byc konfigurowane i testowane na etapie "FCT" przy produkcji
    ETRX357_Set_EchoDisableBaudrate115200();

    vTaskDelay(30);

    //! wylaczenie zbednych promptow od ukladu:
    ETRX357_PromptDisable( ETRX_PROMPT_DISABLE_SEQ | ETRX_PROMPT_DISABLE_ACK | ETRX_PROMPT_DISABLE_NACK | ETRX_PROMPT_DISABLE_SR);

    vTaskDelay(30);

    //! Internal timer2 unused disable (default function - disconnect from empty network - unused in pyro):
    uint16_t timer2OffValue = 0x00; /* from datasheet */
    ETRX357_WriteRegisterValue(0x2e, (uint8_t *)&timer2OffValue, sizeof(timer2OffValue));

    vTaskDelay(50);

    ZB_SayHelloToMaster();

    vTaskDelay(15);

    if(actualConnectionState != ZB_ConnectionState_NoNetwork && actualConnectionState != ZB_ConnectionState_MASTER)
    {
        ZB_Response_Enable();
    }
}

void ZB_CreateNewNetwork(void)
{
    ETRX357_AutojoinEnable();

    vTaskDelay(10);

    ETRX357_Network_Create();

    actualConnectionState = ZB_ConnectionState_Creating;
}

uint16_t ZB_GetMyID(void)
{
    return actualNetworkSettings.ID;
}

const uint8_t* ZB_GetDeviceMAC(void)
{
    return (uint8_t*)actualNetworkSettings.myMAC;
}

const uint8_t* ZB_get_actual_master_MAC_q(void)
{
    return (uint8_t*)(*actualMasterMAC);
}

const uint8_t* ZB_GetMainMasterMAC(void)
{
    return (uint8_t*)&actualNetworkSettings.mainMaster;
}

const uint8_t* ZB_GetSecondaryMaster1_MAC(void)
{
    return (uint8_t*)&actualNetworkSettings.secondaryMaster_1;
}

const uint8_t* ZB_GetSecondareMaster2_MAC(void)
{
    return (uint8_t*)&actualNetworkSettings.secondaryMaster_2;
}

const uint8_t* ZB_GetSecondaryMaster3_MAC(void)
{
    return (uint8_t*)&actualNetworkSettings.secondaryMaster_3;
}

const PID* ZB_GetNetworkPID(void)
{
    return &actualNetworkSettings.myNetwork;
}

void ZB_TEST_SaveFakeNetworkDataToNVS(const ZB_NetworkSettings_s *settings)
{
    uint32_t *wsk = (uint32_t*)settings;
    uint16_t tmp;

    flash_Unlock();

    flash_ErasePage(ZIGBEE_FLASH_SETTINGS_ADDRESS);

    for(tmp = 0; tmp < sizeof(ZB_NetworkSettings_s); tmp += 4)
    {
        flash_Write(ZIGBEE_FLASH_SETTINGS_ADDRESS + tmp, *wsk);
        wsk++;
    }

    flash_Lock();
}

void ZB_SendBroadcastMessage(const char *msg)
{
    ETRX357_SendBroadcast(0U, msg);
}

void ZB_SecondaryMaster_Add(const char *mac, uint8_t slot)
{
    bool saveNewDataToNVS = true;

    switch(slot)
    {
        case 0U:
        {
            strncpy((char*)actualNetworkSettings.secondaryMaster_1, mac, sizeof(MAC));

            break;
        }
        case 1U:
        {
            strncpy((char*)actualNetworkSettings.secondaryMaster_2, mac, sizeof(MAC));

            break;
        }
        case 2U:
        {
            strncpy((char*)actualNetworkSettings.secondaryMaster_3, mac, sizeof(MAC));

            break;
        }
        default:
        {
            /* wrong slot value */
            saveNewDataToNVS = false;

            break;
        }
    }

    if(saveNewDataToNVS)
    {
        ZB_SaveNetworkSettingToNVS();
    }
}

void ZB_SecondaryMasters_Clear(void)
{
    memset(actualNetworkSettings.secondaryMaster_1, 0xff, sizeof(MAC));
    memset(actualNetworkSettings.secondaryMaster_2, 0xff, sizeof(MAC));
    memset(actualNetworkSettings.secondaryMaster_3, 0xff, sizeof(MAC));

    ZB_SaveNetworkSettingToNVS();

    memset(actualNetworkSettings.secondaryMaster_1, 0, sizeof(MAC));
    memset(actualNetworkSettings.secondaryMaster_2, 0, sizeof(MAC));
    memset(actualNetworkSettings.secondaryMaster_3, 0, sizeof(MAC));
}

void ZB_SetTemporaryMainMaster(const char *mac)
{
    strncpy(actualNetworkSettings.mainMaster, mac, sizeof(MAC));

    actualMasterMAC = (MAC*)actualNetworkSettings.mainMaster;
}

uint16_t ZB_GetID(void)
{
    return actualNetworkSettings.ID;
}

void ZB_GetSecondaryMastersString(char buff[50])
{
    sprintf(buff, "%s,%s,%s", actualNetworkSettings.secondaryMaster_1, actualNetworkSettings.secondaryMaster_2, actualNetworkSettings.secondaryMaster_3);
}

void ZB_GetParamsAsString(char buff[80])
{
    sprintf(buff, "%s,%s,%s,%s,%s", (char*)ZB_GetMainMasterMAC(), (char*)ZB_GetSecondaryMaster1_MAC(), (char*)ZB_GetSecondareMaster2_MAC(), (char*)ZB_GetSecondaryMaster3_MAC(), (char*)ZB_GetNetworkPID());
}

void ZB_ClearActualMaster(void)
{
    actualMasterMAC = NULL;
}
