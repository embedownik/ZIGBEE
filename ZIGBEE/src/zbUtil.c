#include <zbUtil.h>

#include <ZIGBEE.h>
#include <ZIGBEE_Response.h>

#include <msgid.h>

#define MAX_DATA_STING_LEN (40)

void zbUtil_SendResponseWithDataCallback_WithoutParams(const char* cmd, char** params, uint8_t paramsCount, zbUtil_GetDataStrCb_t getDataCb)
{
    const uint8_t MSGID_POSITION_IN_PARAMS = 0U;

    uint8_t msgID = msgid_DecodeFromHexString(params[MSGID_POSITION_IN_PARAMS]);

    char dataStringBuff[MAX_DATA_STING_LEN] = "";

    getDataCb(dataStringBuff);

    ZB_Response_SendData(msgID, dataStringBuff);
}

void zbUtil_SendResponseWithDataCallback_WithParams(const char* cmd, char** params, uint8_t paramsCount, zbUtil_GetDataStrWithCommandParseCb_t getDataCb)
{
    const uint8_t MSGID_POSITION_IN_PARAMS = 0U;

    uint8_t msgID = msgid_DecodeFromHexString(params[MSGID_POSITION_IN_PARAMS]);

    char dataStringBuff[MAX_DATA_STING_LEN] = "";

    getDataCb(dataStringBuff, params + 1U, paramsCount - 1U);

    ZB_Response_SendData(msgID, dataStringBuff);
}

void zbUtil_ExecuteCommand_WithParams_InstantResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandCb_t commandCb)
{
    /* params: <MSGID>.....<DecodedInCallbackNotHere> */

    if(paramsCount >= 1U)
    {
        uint8_t msgID = msgid_DecodeFromHexString(params[0]);

        /* parametrs modified because msgID was decoded earlier */
        if(commandCb(params + 1U, paramsCount - 1U))
        {
            ZB_Response_SendOK(msgID);
        }
        else
        {
            zbUtil_CommandParseError();
        }
    }
    else
    {
        zbUtil_CommandParseError();
    }
}

void zbUtil_ExecuteCommand_WithParams_DelayedResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandCb_t commandCb)
{
    /* params: <MSGID>.....<DecodedInCallbackNotHere> */

    if(paramsCount >= 1U)
    {
        uint8_t msgID = msgid_DecodeFromHexString(params[0]);

        /* parametrs modified because msgID was decoded earlier */
        if(commandCb(params + 1U, paramsCount - 1U))
        {
            ZB_Response_SendOK_WithDelay(msgID);
        }
        else
        {
            zbUtil_CommandParseError();
        }
    }
    else
    {
        zbUtil_CommandParseError();
    }
}

void zbUtil_ExecuteCommand_WithoutParams_InstantResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandWithoutParamsCb_t commandCb)
{
    /* params: <MSGID> */

    const uint8_t CMD_WITHOUT_ADDITIONAL_PARAMS = 1U;

    if(paramsCount == CMD_WITHOUT_ADDITIONAL_PARAMS)
    {
        uint8_t msgID = msgid_DecodeFromHexString(params[0]);

        if(commandCb())
        {
            ZB_Response_SendOK(msgID);
        }
        else
        {
            zbUtil_CommandParseError();
        }
    }
}

void zbUtil_ExecuteCommand_WithoutParams_DelayedResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandWithoutParamsCb_t commandCb)
{
    /* params: <MSGID> */

    const uint8_t CMD_WITHOUT_ADDITIONAL_PARAMS = 1U;

    if(paramsCount == CMD_WITHOUT_ADDITIONAL_PARAMS)
    {
        uint8_t msgID = msgid_DecodeFromHexString(params[0]);

        if(commandCb())
        {
            ZB_Response_SendOK_WithDelay(msgID);
        }
        else
        {
            zbUtil_CommandParseError();
        }
    }
}

void zbUtil_CommandError(char *buff)
{
    //debug_Printf("Wrong command");
}

void zbUtil_CommandParseError(void)
{
    //debug_Printf("Command Parse Error!");
}
