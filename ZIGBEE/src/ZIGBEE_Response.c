#include <string.h>

#include <ZIGBEE.h>
#include <ZIGBEE_Response.h>

#include <utils.h>

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>

#include <msgid.h>

#include <status.h>

#define CONFIRM_ARRAY_SIZE (10U)

#define CONFIRM_TASK_TICK (20U)

static const uint8_t COMMAND_QUEUE_SEND_TIME = 5U; /* not important in this case - only for RTOS queue interface */

typedef enum
{
    commandType_AddOK,
    commandType_AddPing,
    commandType_Enable,
    commandType_Disable,
    commandType_ClearAllWithDelay,
}commandType_e;

typedef enum
{
    responseType_OK,
    responseType_ERR,
    responseType_Ping,
}responseType_e;

typedef struct
{
    commandType_e commandType_AddConfirm;
    uint8_t msgID;
    bool    isPingResponse;
}messageQueueCommand_s;

/**
 * Info about "slot" with response to master.
 */
typedef struct
{
    uint8_t msgID;
    responseType_e type;
	bool    slotInUse;
	uint16_t targetTimeCounter;
	uint16_t actualTimeCounter;
}responseInfo_s;

static QueueHandle_t messageQueue;

static const uint8_t confirmQueueSize = 5U;

/**
 * Only task function has direct access to these resources.
 */
static responseInfo_s responsesArray[CONFIRM_ARRAY_SIZE] = { 0 };

static uint8_t activeResponsesCounter = 0U;

static uint8_t findFreeSlotNumber(void)
{
	uint8_t slotNumber = 0U;

	for(uint8_t i = 0U; i < CONFIRM_ARRAY_SIZE; i++)
	{
		if(responsesArray[i].slotInUse != true)
		{
			slotNumber = i;
			break;
		}
	}

	return slotNumber;
}

static uint16_t calculateResponseTimeMs(void)
{
    uint16_t myID = ZB_GetMyID();

    uint16_t responseTime;

    if(myID == 99U) /* 99 is main master indicator */
    {
        responseTime = ZIGBEE_PING_DELAY_TIME_CONSTANT_MS - ZIGBEE_PING_DELAY_TIME_PER_DEVICE_MS;
    }
    else
    {
        responseTime = ZIGBEE_PING_DELAY_TIME_CONSTANT_MS + (myID * ZIGBEE_PING_DELAY_TIME_PER_DEVICE_MS);
    }

    return responseTime;
}

static void sendEventToCommandQueue(messageQueueCommand_s* event)
{
    xQueueSend(messageQueue, (void *)event, COMMAND_QUEUE_SEND_TIME);
}

static bool getEventFromQueue(messageQueueCommand_s *command)
{
    bool retVal = false;

    if(xQueueReceive(messageQueue, command, 0U) == pdPASS)
    {
        retVal = true;
    }

    return retVal;
}

static void ZB_Response_SendOK_Network(uint8_t messageID)
{
    char buff[] = "+OK=xx,xx";
    const uint8_t MSGID_POSITION_IN_MSG = 4U;
    const uint8_t ID_POSITION_IN_MSG = 7U;

    utils__8bits_to_2hex_ascii(messageID, buff + MSGID_POSITION_IN_MSG);
    utils__uint8_to_2_char(ZB_GetID(), buff + ID_POSITION_IN_MSG);

    ZB_SendMessageToMaster(buff);
}

static void ZB_Response_Task(void* params)
{
    (void)params;

    bool isActive = false;

    messageQueueCommand_s actualCommand;

    while(true)
    {
        if(getEventFromQueue(&actualCommand))
        {
            switch(actualCommand.commandType_AddConfirm)
            {
                case commandType_AddOK:
                {
                    uint8_t position = findFreeSlotNumber();

                    responsesArray[position].slotInUse = true;
                    responsesArray[position].targetTimeCounter = calculateResponseTimeMs() / CONFIRM_TASK_TICK;
                    responsesArray[position].type = responseType_OK;
                    responsesArray[position].msgID = actualCommand.msgID;
                    responsesArray[position].actualTimeCounter = 0U;

                    activeResponsesCounter++;

                    break;
                }
                case commandType_AddPing:
                {
                    uint8_t position = findFreeSlotNumber();

                    responsesArray[position].slotInUse = true;
                    responsesArray[position].targetTimeCounter = calculateResponseTimeMs() / CONFIRM_TASK_TICK;
                    responsesArray[position].type = responseType_Ping;
                    responsesArray[position].msgID = actualCommand.msgID;
                    responsesArray[position].actualTimeCounter = 0U;

                    activeResponsesCounter++;

                    break;
                }
                case commandType_ClearAllWithDelay:
                {
                    for(uint8_t i = 0U; i < CONFIRM_ARRAY_SIZE; i++)
                    {
                        responsesArray[i].slotInUse = false;
                    }

                    activeResponsesCounter = 0U;

                    break;
                }
                case commandType_Disable:
                {
                    isActive = false;

                    for(uint8_t i = 0U; i < CONFIRM_ARRAY_SIZE; i++)
                    {
                        responsesArray[i].slotInUse = false;
                    }

                    activeResponsesCounter = 0U;

                    break;
                }
                case commandType_Enable:
                {
                    isActive = true;
                    break;
                }
                default:
                {
                    /* unsupported value */
                    break;
                }
            }
        }

        if(isActive)
        {
            if(activeResponsesCounter != 0U)
            {
                for(uint8_t i = 0U; i < CONFIRM_ARRAY_SIZE; i++)
                {
                    if(responsesArray[i].slotInUse == true)
                    {
                        responsesArray[i].actualTimeCounter++;

                        if(responsesArray[i].actualTimeCounter == responsesArray[i].targetTimeCounter)
                        {
                            switch(responsesArray[i].type)
                            {
                                case responseType_OK:
                                {
                                    ZB_Response_SendOK_Network(responsesArray[i].msgID);

                                    break;
                                }
                                case responseType_Ping:
                                {
                                    ZB_Response_SendPing();

                                    break;
                                }
                                default:
                                {
                                    /* unsupported value */
                                    break;
                                }
                            }

                            responsesArray[i].slotInUse = false;
                            activeResponsesCounter--;
                        }
                    }
                }
            }
        }

        vTaskDelay(CONFIRM_TASK_TICK);
    }
}

void ZB_Response_Init(void)
{
    xTaskCreate(
        ZB_Response_Task,  /* Function that implements the task. */
        "ZBRSP",           /* Text name for the task. */
        200U,               /* Stack size in words, not bytes. */
        (void *) NULL,     /* Parameter passed into the task. */
        6U,                /* Priority at which the task is created. */
        NULL);             /* Used to pass out the created task's handle. */

    /* ta kolejka w praktyce bedzie przechowywac max 1/2 elementy - poniewaz rzeczy z odpowiedziami sa dodawane tylko z kontekstu odbioru ZB */
    const uint8_t queueSize = 2U;

    messageQueue = xQueueCreate(queueSize, sizeof(messageQueueCommand_s));
}

void ZB_Response_SendOK(uint8_t messageID)
{
    char buff[] = "+OK=xx";
    const uint8_t ID_POSITION_IN_MSG = 4U;

    utils__8bits_to_2hex_ascii(messageID, buff + ID_POSITION_IN_MSG);

    ZB_SendMessageToSecondaryMaster(buff);
}

void ZB_Response_SendOK_WithDelay(uint8_t messageID)
{
    messageQueueCommand_s command;

    command.commandType_AddConfirm = commandType_AddOK;
    command.msgID = messageID;

    sendEventToCommandQueue(&command);
}

void ZB_Response_SendData(uint8_t messageID, const char* response)
{
    char responseBuff[100] = "+OK=xx";
    const uint8_t ID_POSITION_IN_MSG = 4U;

    msgid_ConvertToHexString(messageID, responseBuff + ID_POSITION_IN_MSG);

    char* catPointer = strcat(responseBuff, ",");
    catPointer = strcat(catPointer, response);

    ZB_SendMessageToSecondaryMaster(responseBuff);
}

void ZB_Response_SendPing(void)
{
#define STATUS_POSITION  (3U)

    char ping_rsp[STATUS_MAX_LEN + STATUS_POSITION] = "+P=";

    status_GetStatusText(ping_rsp + STATUS_POSITION);

    ZB_SendMessageToMaster(ping_rsp);
}

void ZB_Response_SendPing_WithDelay(void)
{
    messageQueueCommand_s command;

    command.commandType_AddConfirm = commandType_AddPing;

    sendEventToCommandQueue(&command);
}

void ZB_Response_Disable(void)
{
    messageQueueCommand_s command;

    command.commandType_AddConfirm = commandType_Disable;

    sendEventToCommandQueue(&command);
}

void ZB_Response_Enable(void)
{
    messageQueueCommand_s command;

    command.commandType_AddConfirm = commandType_Enable;

    sendEventToCommandQueue(&command);
}

void ZB_Response_ClearAllResponsesWithDelay(void)
{
    messageQueueCommand_s command;

    command.commandType_AddConfirm = commandType_ClearAllWithDelay;

    sendEventToCommandQueue(&command);
}
