/*
 * zbUtil.h
 *
 *      Author: embedownik
 *
 *  Jest to odpowiednik modulu pcUtil. (glowna roznica jest to, ze zbUtil zawsze potrzebuje parametrow z komendy - poniewaz zawsze jest conajmniej 1 - MSG_ID).
 *
 *  Zawiera funkcje pozwalajace uzyskac generycznosc obslugi otrzymanych z sieci komend.
 *
 *   Z powodu tego, ze ten modul zostal "nadbudowany" na inne moduly - niektore funkcje wykonawcze z innych modulow nie sa dostosowane do
 *   typu callbacka jakiego wymaga ta biblioteka - dla nich wewnatrz biblioteki zaimplementowane sa wrappery wewnatrz funkcji
 *   (tzw. "nested functions" - nie jest to w ISO C - tylko jako extension).
 *
 */

#ifndef ZIGBEE_ZIGBEE_INC_ZBUTIL_H_
#define ZIGBEE_ZIGBEE_INC_ZBUTIL_H_

#include <stdint.h>
#include <stdbool.h>

/**
 * Callback for function to return selected parameter as string.
 * Version without additional parameters.
 */
typedef void (*zbUtil_GetDataStrCb_t)(char* buff);

/**
 * Callback for function to return selected parameter as string.
 * Version with additional parameters to parse in callback.
 * returns false in case of parse error.
 */
typedef bool (*zbUtil_GetDataStrWithCommandParseCb_t)(char* buff, char** params, uint8_t paramsCount);

/**
 * Callback for command exetution function - with parameters from command to decode.
 */
typedef bool (*zbUtil_ExecuteCommandCb_t)(char** params, uint8_t paramsCount);

/**
 * Callback for command exetution function - with parameters from command to decode.
 */
typedef bool (*zbUtil_ExecuteCommandWithoutParamsCb_t)(void);


/**
 * Send response to netowrk, but get data to send from callback.
 * Function for getters without parameters.
 */
void zbUtil_SendResponseWithDataCallback_WithoutParams(const char* cmd, char** params, uint8_t paramsCount, zbUtil_GetDataStrCb_t getDataCb);

/**
 * Send response to network, but get data to send from callback.
 * Function for getters with additional parameters in command.
 */
void zbUtil_SendResponseWithDataCallback_WithParams(const char* cmd, char** params, uint8_t paramsCount, zbUtil_GetDataStrWithCommandParseCb_t getDataCb);

/**
 * Execute command (with parameters parse) and send confirm/error to network.
 */
void zbUtil_ExecuteCommand_WithParams_InstantResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandCb_t commandCb);

void zbUtil_ExecuteCommand_WithParams_DelayedResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandCb_t commandCb);

/**
 * Execute command (without additional parameters) and send confirm/error to network.
 */
void zbUtil_ExecuteCommand_WithoutParams_InstantResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandWithoutParamsCb_t commandCb);

void zbUtil_ExecuteCommand_WithoutParams_DelayedResponse(const char* cmd, char** params, uint8_t paramsCount, zbUtil_ExecuteCommandWithoutParamsCb_t commandCb);

/**
 * W przypadku sieci ZB nie odsylamy errorow po sieci - zeby nie zasmiecac pasma itp.
 * a skoro nie powiodlo sie jakies parsowanie to nie wiemy czy inne dane sa dobre
 * podejscie jak w komendach AT - tylko "ogolny" error bez szczegolow
 */
void zbUtil_CommandError(char *buff);

/**
 * W przypadku sieci ZB nie odsylamy errorow po sieci - zeby nie zasmiecac pasma itp.
 * a skoro nie powiodlo sie jakies parsowanie to nie wiemy czy inne dane sa dobre
 * podejscie jak w komendach AT - tylko "ogolny" error bez szczegolow
 */
void zbUtil_CommandParseError(void);

#endif
