/*
 * ZIGBEE_Respones.h
 *
 *      Author: embedownik
 *
 *
 *  Mechanizm odsylania odpowiedzi do nadajnika w sieci.
 *
 *  Zarowno w wersji "odeslij natychmiast" jak i w wersji z opoznieniem (dla komend broadcastowych) oraz odeslanie odpowiedzi na PING.
 *
 */
#ifndef ZIGBEE_ZIGBEE_RESPONSE_H_
#define ZIGBEE_ZIGBEE_RESPONSE_H_

#include <ZIGBEE.h>

void ZB_Response_Init(void);

void ZB_Response_SendOK(uint8_t messageID);

void ZB_Response_SendOK_WithDelay(uint8_t messageID);

void ZB_Response_SendPing(void);

void ZB_Response_SendPing_WithDelay(void);

void ZB_Response_SendData(uint8_t messageID, const char* response);

void ZB_Response_Disable(void);

void ZB_Response_Enable(void);

/**
 * Remove from queue all responses with delay.
 */
void ZB_Response_ClearAllResponsesWithDelay(void);

#endif
