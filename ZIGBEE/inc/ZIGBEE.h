/*
 * ZIGBEE.h
 *
 *      Author: Embedownik
 *
 *  Zigbee Layer for Pyrolligent devices.
 */

#ifndef ZIGBEE_ZIGBEE_H_
#define ZIGBEE_ZIGBEE_H_

#include <stdint.h>

#include <ETRX357.h>
#include "ZIGBEE_defs.h"

/*
 * All ZB states for devices
 */
typedef enum ZB_stan_polaczenia_e
{
    ZB_ConnectionState_NoNetwork,       // calkowicie nie mamy w pamieci danych odnosnie sieci itp
    ZB_ConnectionState_Searching,       // szukamy wlasnie sieci ZB - weszlismy w tryb laczenia
    ZB_ConnectionState_Creating,        // tworzymy wlasnie siec ZB - w przypadku nadajnika
    ZB_ConnectionState_NO_MASTER,       // jestesmy w sieci, ale nie mamy mastera przypisanego, jest to tozsame z brakiem IDka
    ZB_ConnectionState_NO_ID,           // jestesmy w sieci, ale nie mamy IDka przypisanego
    ZB_ConnectionState_Lost,            // "zgubilismy" polaczenie z nadajnikiem, ktore wczesniej nawiazalismy
    ZB_ConnectionState_MasterSearching, // szukamy mastera sieci - wlaczylismy urzadzenie, ktore ma siec i nie polaczyl sie jeszcze z masterem
    ZB_ConnectionState_OK,              // jestemy w sieci ZB i mamy polaczenie z nadajnikiem
    ZB_ConnectionState_MASTER,          // jestesmy nadajnikiem glowny - rozpoznajemy to po odpowiednim ID
} ZB_ConnectionState_e;

/*
Diagram przejsc stanow dla urzadzenia - polygon to stany ktore moga byc po uruchomieniu
digraph  graphname {
	label = "Diagram stanow komunikacji sieciowej";
	labelloc = "t";

    a [label="ZB_no_network"];
    b [label="ZB_searching"];
    c [label="NO_MASTER"];
    d [label="NO_ID"];
    e [label="lost"];
    f [label="master_searching"];
    g [label="OK"];

    a [shape=polygon];
    b [shape=box];
    c [shape=polygon];
    d [shape=polygon];
    e [shape=box];
    f [shape=polygon];
    g [shape=box];

    a -> b [label="uruchomiono szukanie sieci"];
    b -> a [label="wyjscie z szukania sieci\r lub niepolaczenie z zadna"];
    b -> c [label="polaczono z siecia"];
    c -> d [label="Odebrano przywitanie\r od mastera (ramka MASTER)"];
    d -> g [label="Otrzymano IDka"];
    g -> e [label="timeout"];
    e -> g [label="Dotarla ramka"];
    f -> g [label="dostalismy jakas wiadomosc"];
}
*/

/**
 * All network settings
 */
typedef struct
{
	uint16_t ID;		   //! nasz aktualny ID w sieci - 0 -> brak ID
	MAC mainMaster;	       //! MAC nadajnika glownego, zawsze domyslny po uruchomieniu, to on nas dodal do sieci
	MAC secondaryMaster_1; //! MAC nadajnika pobocznego, ktory moze przejac nad nia kontrole
	MAC secondaryMaster_2; //! MAC nadajnika pobocznego, ktory moze przejac nad nia kontrole
	MAC secondaryMaster_3; //! MAC nadajnika pobocznego, ktory moze przejac nad nia kontrole
	MAC myMAC;             //! MAC urzadzenia, zeby nie musiec potem odpytywac ETRXa o to w razie potrzeby
	PID myNetwork;         //! PID sieci do ktorej jestesmy podlaczeni aktualnie
}ZB_NetworkSettings_s;

/**
 * Type for "shortMAC" - copy only last part of MAC
 * +1 for '\0'
 */
typedef char sMAC[5];

/**
 * Typedef for function to display network params.
 */
typedef void (*ZB_cb_NetworkParams_t)(ZIGBEE_NETWORK_PARAMS *);

/**
 * Typedef for function called after "networkTimeout" event.
 */
typedef void (*ZB_cb_Timeout_t)(void);

/**
 * Typedef for function called after "networkConnected" event.
 */
typedef void (*ZB_cb_networkConnected_t)(void);

/**
 * Typedef for function called after "broadcastReceived" event.
 */
typedef void (*ZB_cb_BroadcastReceived_t)(MAC *mac, char *msg);

/**
 * Typedef for function called after "unicastReceived" event.
 */
typedef void (*ZB_cb_UnicastReceived_t)(MAC *mac, char *msg);

/**
 * Typedef for function called after "dataFromNewMasterReceived" event.
 */
typedef void (*ZB_cb_MasterChanged_t)(void);

/**
 * Typedef for function called after "networkScanCompleted" event.
 */
typedef void (*ZB_cb_NetworkScanFinished_t)(ZIGBEE_NETWORK_PARAMS *networks, uint8_t networksNumber);

/**
 * Typedef for function called after "newDeviceInNetwork" event.
 */
typedef void (*ZB_cb_NewDeviceInNetwork_t)(MAC *mac);

/**
 * Typedef for function called after "connectionFailed" event.
 */
typedef void (*ZB_cb_ConnectionFailed_t)(void);

/**
 * Typedef for function for communication with ETRX module.
 */
typedef void (*ZB_cb_SendMSGToETRX_rn_t)(char *msg);

/**
 * All networks callbacks.
 */
typedef struct
{
	ZB_cb_NetworkParams_t       zb_cb_NetworkParams;
	ZB_cb_Timeout_t             zb_cb_Timeout;
	ZB_cb_networkConnected_t    zb_cb_NetworkConnected;
	ZB_cb_BroadcastReceived_t   zb_cb_BroadcastReceived;
	ZB_cb_UnicastReceived_t     zb_cb_UnicastReceived;
	ZB_cb_MasterChanged_t       zb_cb_MasterChanged;
	ZB_cb_NetworkScanFinished_t zb_cb_NetworkScanFinished;
	ZB_cb_NewDeviceInNetwork_t  zb_cb_NewDeviceInNetwork;
	ZB_cb_ConnectionFailed_t    zb_cb_ConnectionFailed;
}ZB_EventsCallbacks_s;

/**
 * ZB network Init.
 */
void ZB_Init(ZB_cb_SendMSGToETRX_rn_t send_rn);

/**
 * Assign all network events callbacks.
 */
void ZB_RegisterCallbacks(ZB_EventsCallbacks_s *events);

/**
 * MASTER ONLY!!!
 *
 * Create new network.
 */
void ZB_CreateNewNetwork(void);

/**
 * "Module" tick - for time measure etc.
 */
void ZB_CheckEvents(void);

/**
 * Start Scan network procedure.
 * Response in callbacks.
 */
void ZB_ScanNetworks_Command(void);

/**
 * Start connection with selected network.
 * @param index number of selected network from callback
 */
void ZB_ConnectWithSelectedNetworkIndex(uint8_t index);

/**
 * Disconnect from network + clear all network settings.
 */
void ZB_Disconnect(void);

/**
 * Command to "getNetworkData" procedure.
 * @param callback function to call in case of event
 */
void ZB_GetNetworkData_Command(ZB_cb_NetworkParams_t callback);

/**
 * @param state to set
 */
void ZB_SetConnectionState(ZB_ConnectionState_e state);

/**
 * @return actual connection state
 */
ZB_ConnectionState_e ZB_GetActualConnectionState(void);

/**
 * Send message to actual master.
 */
void ZB_SendMessageToMaster(const char* wsk);

/**
 * Send message to secondary master - for confirmations.
 */
void ZB_SendMessageToSecondaryMaster(const char *wsk);

/**
 * To call from device menu during test phase.
 */
void ZB_SayHelloToMaster_TEST(void);

/**
 * Info that device is turn on.
 * Only in proper connection state.
 */
void ZB_SayHelloToMaster(void);

/**
 * Send Hello to master after Master request (without additional conditions).
 */
void ZB_SayHelloToMasterResponse(void);

/**
 * Clear all setting about network in NVS.
 */
void ZB_SettingsClearInNVS(void);

/**
 * @return myID
 */
uint16_t ZB_GetMyID(void);

/**
 * @return myMAC address
 */
const uint8_t* ZB_GetDeviceMAC(void);

/**
 * @return main master MAC
 */
const uint8_t* ZB_GetMainMasterMAC(void);

/**
 * @return secondary master 1 MAC
 */
const uint8_t* ZB_GetSecondaryMaster1_MAC(void);

/**
 * @return secondary master 2 MAC
 */
const uint8_t* ZB_GetSecondareMaster2_MAC(void);

/**
 * @return secondary master 3 MAC
 */
const uint8_t* ZB_GetSecondaryMaster3_MAC(void);

/**
 * @return actual network PID
 */
const PID* ZB_GetNetworkPID(void);

/**
 * Just for dev phase.
 */
void ZB_TEST_SaveFakeNetworkDataToNVS(const ZB_NetworkSettings_s *settings);

/**
 * Verify if given MAC is MAC from master settings.
 * Change actual master in case of message from differen device.
 * @return
 */
bool ZB_CheckIfGivenMACAddressIsMaster_WithMasterChange(const MAC *mac);

/**
 * Verify if given MAC is MAC from master settings.
 * Without change actual master in case of message from differen device.
 * @return
 */
bool ZB_CheckIfGivenMACAddressIsMaster_WithoutMasterChange(const MAC *mac);


/**
 * Call after successfull connection with master.
 */
void ZB_ClearConnectionStatusCounter(void);

/**
 * Save actual network settings from RAM to NVS.
 */
void ZB_SaveNetworkSettingToNVS(void);

/**
 * USED ONLY IN MASTERS!
 * Send broadcast to network.
 */
void ZB_SendBroadcastMessage(const char *msg);

/**
 * Add new secondary master data.
 */
void ZB_SecondaryMaster_Add(const char* mac, uint8_t slot);

/**
 * Clear all Secondary Masters data.
 */
void ZB_SecondaryMasters_Clear(void);

/**
 * Ustawia nadajnik glowny na podany adres - wykorzystywane w nadajniku glownym
 * Do przestawienia chwilowego nadajnika i odpowiadania do niego.
 */
void ZB_SetTemporaryMainMaster(const char *mac);

/**
 * Save selected ID in NVS.
 */
void ZB_SaveIDInNVS(uint16_t ID);

/**
 * Save ID and masterMAC in NVS.
 */
void ZB_SaveMasterMACAndID(uint16_t ID, const MAC *mac);

/**
 * Returns string with info about secondary masters.
 */
void ZB_GetSecondaryMastersString(char buff[50]);

/**
 * @return actual device ID in network.
 */
uint16_t ZB_GetID(void);

/**
 * Convert ZB params to String.
 */
void ZB_GetParamsAsString(char buff[80]);

/**
 * Function to call from task after RTOS init.
 */
void ZB_StartTask(void);

/**
 * Function to call after master change in master devices.
 */
void ZB_ClearActualMaster(void);

#endif
