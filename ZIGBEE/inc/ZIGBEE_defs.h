#ifndef ZIGBEE_ZIGBEE_DEFS_H_
#define ZIGBEE_ZIGBEE_DEFS_H_

#include <debug.h>

/**
 * ID dla glownego nadajnika w sieci.
 */
#define ZB_NETWORK_MASTER_ID (99U)

#define ZB_DEBUG_PRINT(...)

//! PINGOWANIE, opoznione odpowiedzi na broadcasty
//! podczas pingowania, ale takze odpowiadania OKejkami do nadajnika urzadzenia nie moga tego zrobic jednoczesnie
//! poniewaz zablokuja d, dlatego nalezy "rozlozyc" troche ten ruch. Odpowiedzi wystapia po czasie:
//! ZIGBEE_PING_DELAY_TIME_CONSTANT + (ZIGBEE_PING_DELAY_TIME_PER_DEVICE * ID_urzadzenia)
//! tutaj definiujemy te czasy - celowo jest to zrobione w submodule wspolnej biblioteki:
#define ZIGBEE_PING_DELAY_TIME_PER_DEVICE_MS (170UL)
#define ZIGBEE_PING_DELAY_TIME_CONSTANT_MS   (500UL)

//! czas po ktorym uznajemy, ze nie mamy polaczenia z siecia ZB (nadajnik nie odpytuje nas o status, ani nic nie przesyla)
//! oczywiscie tu sa 2 poziomy "sieci" - ogolna siec ZB to warstwa nizsza, a tu mowa o sieci Pyrolligent - poziom wyzej
#define ZB_DISCONNECT_TIME_MS (25000UL)

#endif
